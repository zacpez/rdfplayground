const CopyPlugin = require("copy-webpack-plugin");

// vue.config.js
module.exports = {
    configureWebpack: {
        plugins: [
            new CopyPlugin({
                patterns:[
                    {from: './src/images', to: 'images'},
                    {from: './public/favicon.ico', to: 'favicon.ico'},
                    {from: './public/index.html', to: 'index.html'},
                ]
            })
        ]
    }
}