import {
    getSolidDataset,
    getThing
} from "@inrupt/solid-client";
import { Session } from "@inrupt/solid-client-authn-browser";
/**
 * @class Client
 * @desc This is a solid client wrapper to manage application logic
 * 
 * @var session solid client connection object
 * @var webId URI of expected user
 */
class Client {
    // Set up session data, and redirect handler
    constructor() { 
        this.session = new Session();
        this.webId = '';
        this.handleRedirectAfterLogin()
        .then(() => {
            // eslint-disable-next-line
            console.log('Setup Solid redirect handle');
        })
        .catch((e) => {
            throw e;
        })
    }

    // 1b. Login Redirect. Call session.handleIncomingRedirect() function.
    // When redirected after login, finish the process by retrieving session information.
    async handleRedirectAfterLogin() {
        await this.session.handleIncomingRedirect(window.location.href);
        this.webId = this.session.info.webId;
        const loginEvent = new Event('solid-login');
        document.dispatchEvent(loginEvent);
    }

    // Get profile data
    async readProfile() {
        // Profile is public data; i.e., you do not need to be logged in to read the data.
        // For illustrative purposes, shows both an authenticated and non-authenticated reads.

        let myDataset;
        if (this.session.info.isLoggedIn){
            myDataset = await getSolidDataset(this.webId, { fetch: this.session.fetch });
        } else {
            myDataset = await getSolidDataset(this.webId);
        }

        return getThing(myDataset, this.webId);
    }

    // Login into the chosen Solid provider
    async login(provider) {
        if (!this.session.info.isLoggedIn) {
            await this.session.login({
                oidcIssuer: provider,
                redirectUrl: window.location.href,
            });
        }
    }

    // Logout of the Solid session and reset application state
    // TODO: See if we can maintain this.session instead of deleting it
    async logout() {
        if (this.session.info.isLoggedIn) {
            await this.session.logout();
            delete this.session;
            this.session = new Session();
        }
    }
}
export {
    Client
}