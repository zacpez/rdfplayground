import { createStore, createLogger } from 'vuex';
import solidUser from '@/store/modules/solid-user';
import { Process } from '@/classes/Process';
const debug = Process.isDebug();

export default createStore({
    modules: {
      solidUser
    },
    strict: debug,
    plugins: debug ? [createLogger()] : []
});
