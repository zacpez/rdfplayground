import { VCARD } from "@inrupt/vocab-common-rdf";
import {
    getStringNoLocale,
    getUrl
} from "@inrupt/solid-client";
import { Client } from '@/api/solid/client';
const client = new Client();

const state = {
    fn: '',
    role: '',
    status: '',
    statusRole: '',
    photo: '',
    provider: 'https://inrupt.net',
    isLoggedIn: false,
    webId: ''
};

const getters = {
    provider() {
        return state.provider;
    },
    fn(state){
        return state.fn;
    },
    role(state) {
        return state.role;
    },
    status(state) {
        return state.status;
    },
    statusRole(state) {
        return state.statusRole;
    },
    photo(state) {
        return state.photo;
    },
    isLoggedIn(state) {
        return state.isLoggedIn;
    },
    webId(state) {
        return state.webId;
    }
};

const actions = {
    resetState({ commit }) {
       commit('resetState');
    },
    login({ commit }) {
        if(!state.provider) {
            console.error('No Solid provider selected');
        }
        client.login(state.provider)
            .then((profile) => {
                console.log(profile)
                commit('setProfile', profile);
            }).catch(() => {
                commit('resetState');
            });
    },
    logout({ commit }) {
        client.logout()
            .then(() => {
                console.log('a')
                commit('resetState');
            }).catch(() => {
                console.log('b')
                commit('resetState');
            });
    },
    setUserProvider({ commit }, provider) {
        commit('setProvider', provider);
    },
    async getUserProfile({ commit }) {
        const profile = await client.readProfile();
        commit('setProfile', profile);
    }
};

const mutations = {
    resetState(state) {
        state.fn = '';
        state.role = '';
        state.status = '';
        state.statusRole = '';
        state.photo = '';
        state.isLoggedIn = false;
    },
    setProvider(state, provider) {
        state.provider = provider;
    },
    setProfile(state, profile) {
        console.log(profile)
        // Get the formatted name (fn) using the property identifier "http://www.w3.org/2006/vcard/ns#fn".
        // VCARD.fn object is a convenience object that includes the identifier string "http://www.w3.org/2006/vcard/ns#fn".
        // As an alternative, you can pass in the "http://www.w3.org/2006/vcard/ns#fn" string instead of VCARD.fn.
        state.fn = getStringNoLocale(profile, VCARD.fn);

        // VCARD.role obect is a convenience object that includes the identifier string "http://www.w3.org/2006/vcard/ns#role"
        // As an alternative, you can pass in the "http://www.w3.org/2006/vcard/ns#role" string instead of VCARD.role.
        state.role = getStringNoLocale(profile, VCARD.role);

        state.photo = getUrl(profile, VCARD.hasPhoto);
        if(state.fn) {
            state.status = 'Your session is logged in.';
            state.statusRole = 'alert';
            state.isLoggedIn = true;
        }
    }
};
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};