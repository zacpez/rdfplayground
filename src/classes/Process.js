/**
 * Process class is to facilitate process utility methods
 */
class Process {
    static isDebug() {
        return process.env.NODE_ENV !== 'production';
    } 
}

export {
    Process
}