import { createApp } from 'vue';
import App from '@/App.vue';
import router from '@/router';
import store from '@/store'

const APP_ELEMENT = '#app';
const app = createApp(App);
app.use(router);
app.use(store);
app.mount(APP_ELEMENT);
