import { createRouter, createWebHistory } from 'vue-router';
import HomePage from '@/components/pages/HomePage.vue';

const routes = [
  {
    path: '/',
    name: 'RDF Playground',
    component: HomePage
  },
  {
    path: '/browse',
    name: 'Browse Semantic Web',
    component: () => import(/* webpackChunkName: "browse" */ '@/components/pages/Browse.vue')
  },
  {
    path: '/publish',
    name: 'Publish to the Semantic Web',
    component: () => import(/* webpackChunkName: "publish" */ '@/components/pages/Publish.vue')
  },
  {
    path: '/search',
    name: 'Search Semantic Web',
    component: () => import(/* webpackChunkName: "search" */ '@/components/pages/Search.vue')
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
