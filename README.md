# RDF Playground

Resource Description Framework(RDF) playground is a project for user to personal their view of the semantic web world. Here in this App they can connect their from a [SolidPod](https://inrupt.com/products/enterprise-solid-server) to implement a search, view, or host new data.

## Use Cases
1. Generic search
2. Visualize RDF at higher level
3. Save Views
4. Add to the Semanic web

## For Developers
Requirements:
* Node 12+

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

## References

Here are a number of places being used to implement this project. 

* [solid-client-authn:](https://docs.inrupt.com/developer-tools/javascript/client-libraries/) Authentication client
* [Graphy:](https://docs.inrupt.com/developer-tools/javascript/client-libraries/) Client-side RDF operations
* [W3C:](https://www.w3.org/TR/?tag=data) Collection of related specs
* [Vue 3:](https://v3.vuejs.org/guide/) Frontend framework
